package com.example.blog.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FileStorageServiceTest {
    @Mock
    private AmazonS3 amazonS3;
    @InjectMocks
    private FileStorageService fileStorageService;

    @Test
    void testUploadImageInProductionEnvironment() throws IOException {
        // Given
        String bucketName= fileStorageService.BUCKET_NAME;
        MultipartFile image = new MockMultipartFile("image", "image.jpg".getBytes());
        String filePath= "blog-images/image_u3403404343e.jpg";
        fileStorageService.setIS_PRODUCTION_ENVIRONMENT(true);
        File file = new File(filePath);
        // When
        fileStorageService.uploadImage(image,"image_u3403404343e" );

        // Verify
        ArgumentCaptor<PutObjectRequest> requestCaptor = ArgumentCaptor.forClass(PutObjectRequest.class);
        verify(amazonS3).putObject(requestCaptor.capture());
        PutObjectRequest putObjectRequest = requestCaptor.getValue();
        assertEquals(putObjectRequest.getBucketName(), bucketName);
        assertEquals(putObjectRequest.getKey(), filePath);
        assertEquals(putObjectRequest.getFile(), file);

    }

    @Test
    void testUploadContentInProductionEnvironment() throws IOException {
        // Given
        String bucketName= fileStorageService.BUCKET_NAME;
        String content ="<p>Random content</p>";
        String fileName = "content_983ur0833";
        String contentPath = "blog-content/"+ fileName + ".txt";
        fileStorageService.setIS_PRODUCTION_ENVIRONMENT(true);
        File file = new File(contentPath);

        // When
        fileStorageService.uploadContent(content,fileName);

        // Verify
        ArgumentCaptor<PutObjectRequest> requestCaptor = ArgumentCaptor.forClass(PutObjectRequest.class);
        verify(amazonS3).putObject(requestCaptor.capture());
        PutObjectRequest putObjectRequest = requestCaptor.getValue();
        assertEquals(bucketName,putObjectRequest.getBucketName());
        assertEquals(contentPath, putObjectRequest.getKey());
        assertEquals(file, putObjectRequest.getFile());
    }

    @Test
    void testGetImageByFileNameInProductionEnvironment() throws IOException {
        String bucketName= fileStorageService.BUCKET_NAME;
        String filePath= "blog-images/image_u3403404343e.jpg";
        String fileName= "image_u3403404343e";
        fileStorageService.setIS_PRODUCTION_ENVIRONMENT(true);
        byte[] expectedBytes = {0x12, 0x34, 0x56, 0x78};
        S3Object s3Object = new S3Object();
        s3Object.setBucketName(bucketName);
        s3Object.setKey(filePath);
        s3Object.setObjectContent(new ByteArrayInputStream(expectedBytes));
        when(amazonS3.getObject(bucketName, filePath))
                .thenReturn(s3Object);
        // Then
        byte[] actualBytes= fileStorageService.getImageByFileName(fileName);
        // Verify
        assertArrayEquals(expectedBytes, actualBytes);
        verify(amazonS3).getObject(bucketName, filePath);
    }

    @Test
    void testGetContentByFileNameInProductionEnvironment() throws IOException {
        String bucketName= fileStorageService.BUCKET_NAME;
        String filePath= "blog-content/content_u3403404343e.txt";
        String fileName= "content_u3403404343e";
        fileStorageService.setIS_PRODUCTION_ENVIRONMENT(true);
        byte[] expectedBytes = {0x48, 0x65, 0x6c, 0x6c, 0x6f};
        S3Object s3Object = new S3Object();
        s3Object.setBucketName(bucketName);
        s3Object.setKey(filePath);
        s3Object.setObjectContent(new ByteArrayInputStream(expectedBytes));
        when(amazonS3.getObject(bucketName, filePath))
                .thenReturn(s3Object);
        // Then
        String actualContent = fileStorageService.getContentByFileName(fileName);
        // Verify
        assertEquals("Hello", actualContent);
        verify(amazonS3).getObject(bucketName, filePath);
    }

    @Test
    void testReadBytesFromStream() throws IOException {
        String testString = "test string";
        byte[] bytes = testString.getBytes();
        InputStream inputStream = new ByteArrayInputStream(bytes);
        assertArrayEquals(bytes, fileStorageService.readBytesFromStream(inputStream));
    }

    @Test
    void testReadBytesFromDisk() throws IOException {
        // create a test file with some content
        byte[] expected = "hello, world!".getBytes();
        Path testFilePath = Files.createTempFile("test", ".txt");
        Files.write(testFilePath, expected);

        // call the method with the path to the test file
        byte[] actual = fileStorageService.readBytesFromDisk(testFilePath);

        // verify that the method returns the expected byte array
        assertArrayEquals(expected, actual);

        // clean up temporary file
        Files.deleteIfExists(testFilePath);
    }

}
