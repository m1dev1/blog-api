package com.example.blog.service;

import com.example.blog.dto.PostDTO;
import com.example.blog.exception.NotFoundException;
import com.example.blog.repository.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.blog.model.Status.ONLINE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PostServiceTest {
    @Mock
    private PostViewRepository postViewRepository;
    @Mock
    private CategoryRepository categoryRepository;
    @Mock
    private FileStorageService fileStorageService;

    @InjectMocks
    private PostService postService;

    @Test
    void testGetAllPosts() {
        // Prepare test data
        List<PostView> posts = new ArrayList<>();
        PostView postView1 = new PostView();
        postView1.setPostId(1);
        postView1.setTitle("Post 1");
        postView1.setSubtitle("Subtitle 1");
        postView1.setStatus("online");
        PostView postView2 = new PostView();
        postView2.setPostId(2);
        postView2.setTitle("Post 2");
        postView2.setSubtitle("Subtitle 2");
        postView2.setStatus("online");
        posts.add(postView1);
        posts.add(postView2);

        when(postViewRepository.findAllByStatus("online")).thenReturn(posts);

        // Call the method being tested
        List<PostDTO> postRespons = postService.getAllPosts();

        // Verify the result
        assertEquals(2, postRespons.size());

        PostDTO postDTO = postRespons.get(0);
        assertEquals(postView1.getTitle(), postDTO.getTitle());
        assertEquals(postView1.getSubtitle(), postDTO.getSubtitle());
        assertEquals(postView1.getCreatedAt(), postDTO.getCreatedAt());
        assertEquals(postView1.getPublishedAt(), postDTO.getPublishedAt());
        assertEquals(postView1.getFavorite(), postDTO.getFavorite());
        assertEquals(postView1.getUrlRoute(), postDTO.getUrlRoute());
        assertEquals(postView1.getCategory(), postDTO.getCategory());
    }

    @Test
    public void testGetBlogPostByUrlRoute() {
        // mock repository response
        PostView postView = new PostView();
        postView.setTitle("Test post");
        postView.setContentName("test-content");
        when(postViewRepository.findByUrlRouteAndStatus("test-urlRoute", ONLINE))
                .thenReturn(Optional.of(postView));

        // mock content response
        String testContent = "Test post content";
        when(postService.getContent(anyString())).thenReturn(testContent);

        // call method
        PostDTO postDTO = postService.getPostByUrlRoute("test-urlRoute");

        // verify that the post was found and returned
        verify(postViewRepository).findByUrlRouteAndStatus("test-urlRoute", ONLINE);
        assertEquals("Test post", postDTO.getTitle());
        assertEquals(testContent, postDTO.getContent());
    }

    @Test
    public void testGetBlogPostByUrlRouteNotFound() {
        // mock repository response
        when(postViewRepository.findByUrlRouteAndStatus("test-urlRoute", ONLINE))
                .thenReturn(Optional.empty());

        // call method and verify that it throws a NotFoundException
        assertThrows(NotFoundException.class, () -> postService.getPostByUrlRoute("test-urlRoute"));

    }

    @Test
    public void testGetFavoritePosts() {
        // prepare test data
        PostView postView1 = new PostView();
        postView1.setTitle("favorite post 1");
        postView1.setContentName("favorite-content 1");
        postView1.setFavorite(true);
        PostView postView2 = new PostView();
        postView2.setTitle("favorite post 2");
        postView2.setContentName("favorite-content 2");
        postView2.setFavorite(true);
        List<PostView> postViewList = List.of(postView1, postView2);
        when(postViewRepository.findByFavoriteAndStatusOrderByPublishedAtDesc(true, "online"))
                .thenReturn(postViewList);

        List<PostDTO> expected = postViewList.stream()
                .map(postView -> {
                    return postService.mapToPostResponse(postView);
                }).toList();

        // call method
        List<PostDTO> actual = postService.getFavoritePosts();

        // verify that the post was found and returned
        assertEquals(expected, actual);

    }

    @Test
    public void testGetSimilarPosts() {
        // Prepare test data
        PostView postView1 = new PostView();
        postView1.setUrlRoute("post-urlRoute-1");
        postView1.setTitle("Post title-1");
        postView1.setCategory("Category-1");
        postView1.setStatus("online");

        PostView postView2 = new PostView();
        postView2.setUrlRoute("post-urlRoute-2");
        postView2.setTitle("Post title-2");
        postView2.setCategory("Category-2");
        postView2.setStatus("online");

        PostView similarPost = new PostView();
        similarPost.setUrlRoute("similar-urlRoute");
        similarPost.setTitle("similar-title");
        similarPost.setCategory("similar-category");
        similarPost.setStatus("online");

        // Mock test data
        when(postViewRepository.findByUrlRouteAndStatus("post-urlRoute-1", ONLINE))
                .thenReturn(Optional.of(postView1));
        when(postViewRepository.findSimilarPosts("Category-1", "online", "post-urlRoute-1"))
                .thenReturn(List.of(similarPost));
        when(postViewRepository.findAll())
                .thenReturn(List.of(postView1,postView2, similarPost));
        // Expected
        List<PostDTO> expected = List.of(similarPost, postView2).stream()
                .map(postView -> {
                    return postService.mapToPostResponse(postView);
                }).toList();
        // Test method with mock post URL
        List<PostDTO> actual = postService.getSimilarPosts("post-urlRoute-1");

        // Then
        assertEquals(expected, actual);

    }
}
