package com.example.blog.service;

import com.example.blog.dto.PostRequest;
import com.example.blog.dto.PostDTO;
import com.example.blog.model.*;
import com.example.blog.exception.AlreadyExistsException;
import com.example.blog.exception.NotFoundException;
import com.example.blog.mapper.PostMapper;
import com.example.blog.repository.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AdminServiceTest {
    @Mock
    private PostRepository postRepository;
    @Mock
    private PostViewRepository postViewRepository;
    @Mock
    private PostImageRepository postImageRepository;
    @Mock
    private PostContentRepository postContentRepository;
    @Mock
    private CategoryRepository categoryRepository;
    @Mock
    private FileStorageService fileStorageService;
    @InjectMocks
    private AdminService adminService;


    @Test
    void testGetAllPosts(){
        // Given
        PostView postView1 = new PostView();
        postView1.setTitle("My First Blog Post");
        postView1.setCreatedAt(Timestamp.valueOf("2023-02-19 13:00:00"));
        postView1.setStatus("offline");
        PostView postView2 = new PostView();
        postView2.setTitle("My Second Blog Post");
        postView2.setSubtitle("An amazing story about my adventures");
        postView2.setUrlRoute("/second-post");
        PostView postView3 = new PostView();
        postView3.setTitle("My Third Blog Post");
        postView3.setCategory("General");
        postView3.setContentName("third-content");
        List<PostView> postViewList = List.of(postView1, postView2, postView3);
        when(postViewRepository.findAll())
                .thenReturn(List.of(postView1, postView2, postView3));
        // When

        List<PostDTO> postRespons = adminService.getAllPosts();
        // Then
        assertEquals(3, postRespons.size());
    }

    @Test
    void testGetPostByUrlRoute() throws IOException {
        // Given
        String urlRoute = "/posts/my-second-blog-post";
        PostView postView = new PostView(2, "My Second Blog Post",
                "An amazing story about my adventures", Timestamp.valueOf("2023-02-18 10:00:00"),
                Timestamp.valueOf("2023-02-18 10:00:00"), "published",
                "/posts/my-second-blog-post", true, "Travel",
                "second-blog-post.jpg", "second-blog-post.md"
        );
        String content = "<p>Here can be a story<p>";
        when(postViewRepository.findByUrlRoute(urlRoute))
                .thenReturn(Optional.of(postView));
        when(fileStorageService.getContentByFileName("second-blog-post.md"))
                .thenReturn(content);

        PostDTO expected = PostMapper.map(postView);
        expected.setContent(content);
        // When
        PostDTO actual = adminService.getPostByPostId(urlRoute);

        // Then
        assertEquals(expected, actual);

    }

    @Test
    void testGetPostByPostUrlWhenUrlRouteIsInvalid(){
        // When
        when(postViewRepository.findByUrlRoute("invalid-url-route"))
                .thenReturn(Optional.empty());
        // Then
        assertThrows(NotFoundException.class, () ->{
           adminService.getPostByPostId("invalid-url-route");
        });
    }

    @Test
    void testGetPostByUrlRouteWhenGetContentFailed() throws IOException {
        // Given
        String urlRoute = "/valentine-day";
        PostView postView = new PostView(4, "Valentine's Day",
                "Valentine's Day Subtitle", Timestamp.valueOf("2023-02-7 10:00:00"),
                Timestamp.valueOf("2023-02-8 10:00:00"), "Online",
                "/valentine-day", true, "Life",
                "valentine-day.jpg", "valentine-day.md"
        );

        // When
        when(postViewRepository.findByUrlRoute(urlRoute))
                .thenReturn(Optional.of(postView));
        when(fileStorageService.getContentByFileName("valentine-day.md"))
                .thenThrow(IOException.class);

        // Then
        assertThrows(RuntimeException.class, () ->{
            adminService.getPostByPostId("/valentine-day");
        });
    }

    @Test
    void testDeletePost(){
        // Given
        String urlRoute = "/delete-this-post";
        Post post = new Post();
        post.setPostId(859);
        post.setTitle("These Post will be deleted");
        post.setPublishedAt(Timestamp.valueOf("2023-04-11 10:00:00"));
        post.setUrlRoute("/delete-this-post");
        PostImage postImage= new PostImage();
        postImage.setPostId(859);
        postImage.setFileName("image-file-name");

        PostContent postContent = new PostContent();
        postContent.setPostId(859);
        postContent.setFileName("content-file-name");

        when(postRepository.findByUrlRoute(urlRoute))
                .thenReturn(Optional.of(post));

        when(postImageRepository.findByPostId(859))
                .thenReturn(Optional.of(postImage));

        when(postContentRepository.findByPostId(859))
                .thenReturn(Optional.of(postContent));

        // When
        adminService.deletePost(urlRoute);

        // Then
        verify(postRepository, times(1)).delete(post);
        verify(postImageRepository, times(1)).delete(postImage);
        verify(postContentRepository, times(1)).delete(postContent);
        verify(fileStorageService, times(1)).deleteImage("image-file-name");
        verify(fileStorageService, times(1)).deleteContent("content-file-name");
    }

    @Test
    void testCreatePost() throws IOException {
        // Prepare test data
        PostRequest postRequest = new PostRequest("Test urlRoute","Test Post", "Test Subtitle",
                "Test Category", "online",true,"Test Content");

        MultipartFile image = new MockMultipartFile("image","image.jpg".getBytes());
        when(postRepository.findByUrlRoute("Test urlRoute"))
                .thenReturn(Optional.empty());
        when(categoryRepository.findByName("Test Category"))
                .thenReturn(Optional.of(new Category(4,"Test Category")));
        // When
        PostDTO result  = adminService.createBlogPost(postRequest, image);

        // Verify the results
        assertEquals("Post created", result);
        ArgumentCaptor<Post> postCaptor = ArgumentCaptor.forClass(Post.class);
        verify(postRepository).save(postCaptor.capture());
        Post savedPost = postCaptor.getValue();
        assertEquals(postRequest.title(), savedPost.getTitle());
        assertEquals(postRequest.subtitle(), savedPost.getSubtitle());
        assertEquals(postRequest.status(), savedPost.getStatus());
        assertEquals(postRequest.urlRoute(), savedPost.getUrlRoute());
        assertEquals(postRequest.favorite(), savedPost.getFavorite());
        assertNotNull(savedPost.getPublishedAt());
        assertNotNull(savedPost.getCreatedAt());

        verify(postImageRepository).save(any(PostImage.class));
        verify(postContentRepository).save(any(PostContent.class));
        verify(fileStorageService).uploadImage(eq(image), anyString());
        verify(fileStorageService).uploadContent(eq(postRequest.content()), anyString());

    }

    @Test
    void testCreatePostWhenUrlAlreadyExists(){
        // Given
        PostRequest postRequest = new PostRequest("Test urlRoute","Test Post", "Test Subtitle",
                "Test Category", "online", true,"Test Content");

        MultipartFile image = new MockMultipartFile("image","image.jpg".getBytes());
        Post existingPost = new Post();
        existingPost.setPostId(64);
        existingPost.setUrlRoute("Test urlRoute");
        when(postRepository.findByUrlRoute("Test urlRoute"))
                .thenReturn(Optional.of(existingPost));

        // Then
        assertThrows(AlreadyExistsException.class, () ->{
            adminService.createBlogPost(postRequest, image);
        });
    }

    @Test
    void testCreatePostWhenCategoryNotFound(){
        // Given
        PostRequest postRequest = new PostRequest("Test urlRoute","Test Post", "Test Subtitle", "Test Category",
                "online", true,"Test Content");

        MultipartFile image = new MockMultipartFile("image","image.jpg".getBytes());
        when(postRepository.findByUrlRoute("Test urlRoute"))
                .thenReturn(Optional.empty());
        when(categoryRepository.findByName("Test Category"))
                .thenReturn(Optional.empty());

        // Then
        assertThrows(NotFoundException.class, ()->{
            adminService.createBlogPost(postRequest, image);
        });
    }


    @Test
    public void testUpdateBlogPost() throws IOException {
        // Prepare test data
        String urlRoute = "https://example.com/post-1";
        PostRequest updateReq = new PostRequest(urlRoute,"New title","New subtitle",
                 "Technology", "online",true,"New content");
        MultipartFile image = new MockMultipartFile("image.jpg", new byte[]{});

        Post existingPost = new Post();
        existingPost.setPostId(851);
        existingPost.setTitle("Old title");
        existingPost.setSubtitle("Old subtitle");
        existingPost.setStatus("offline");
        existingPost.setCreatedAt(Timestamp.valueOf(LocalDateTime.now().minusDays(1)));
        existingPost.setPublishedAt(null);
        existingPost.setFavorite(false);
        existingPost.setUrlRoute(urlRoute);
        existingPost.setCategoryId(4);

        PostImage postImage = new PostImage();
        postImage.setImageId(3);
        postImage.setFileName("image_123456789.jpg");
        postImage.setPostId(existingPost.getPostId());

        PostContent postContent = new PostContent();
        postContent.setContentId(7);
        postContent.setFileName("content_123456789.txt");
        postContent.setPostId(existingPost.getPostId());

        Category category = new Category(15, "Technology");

        when(postRepository.findByUrlRoute(urlRoute)).thenReturn(Optional.of(existingPost));
        when(postImageRepository.findByPostId(existingPost.getPostId())).thenReturn(Optional.of(postImage));
        when(postContentRepository.findByPostId(existingPost.getPostId())).thenReturn(Optional.of(postContent));
        when(categoryRepository.findByName("Technology")).thenReturn(Optional.of(category));
        // When
        String result = adminService.updateBlogPost(updateReq, image);

        // Verify the results
        assertEquals("Post updated", result);

        ArgumentCaptor<Post> postCaptor = ArgumentCaptor.forClass(Post.class);
        verify(postRepository).save(postCaptor.capture());
        Post savedPost = postCaptor.getValue();
        assertEquals(existingPost.getPostId(), savedPost.getPostId());
        assertEquals(updateReq.title(), savedPost.getTitle());
        assertEquals(updateReq.subtitle(), savedPost.getSubtitle());
        assertEquals(updateReq.status(), savedPost.getStatus());
        assertEquals(existingPost.getCreatedAt(), savedPost.getCreatedAt());
        assertNotNull(savedPost.getPublishedAt());
        assertEquals(updateReq.favorite(), savedPost.getFavorite());
        assertEquals(existingPost.getUrlRoute(), savedPost.getUrlRoute());

        verify(fileStorageService).uploadImage(image,  postImage.getFileName());
        verify(fileStorageService).uploadContent(updateReq.content(), postContent.getFileName());
    }


}
