package com.example.blog.repository;

import com.example.blog.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.blog.model.Status.ONLINE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class PostViewRepositoryTest {

    @Autowired
    private PostViewRepository postViewRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private PostImageRepository postImageRepository;
    @Autowired
    private PostContentRepository postContentRepository;


    @BeforeEach
    public void setup() {
        // Create and save some test data
        Category category1 = new Category(null, "category1");
        Category category2 = new Category(null, "category2");
        Category category3 = new Category(null, "category3");
        category1 = categoryRepository.save(category1);
        category2 = categoryRepository.save(category3);
        category3 = categoryRepository.save(category3);
        Post post1 = new Post(null, "title1", "subtitle1",
                Timestamp.valueOf("2023-04-11 10:00:00"), Timestamp.valueOf("2023-04-11 10:00:00"),
                "online", "url1", false, category1.getCategoryId());
        Post post2 = new Post(null, "title2", "subtitle2",
                Timestamp.valueOf("2023-04-12 10:00:00"), Timestamp.valueOf("2023-04-12 10:00:00"),
                "offline", "url2", true, category2.getCategoryId());
        Post post3 = new Post(null, "title3", "subtitle3",
                Timestamp.valueOf("2023-04-13 10:00:00"), null,
                "online", "url3", true, category3.getCategoryId());

        post1= postRepository.save(post1);
        post2= postRepository.save(post2);
        post3= postRepository.save(post3);

        postImageRepository.save(new PostImage("file1", post1.getPostId()));
        postImageRepository.save(new PostImage("file2", post2.getPostId()));
        postImageRepository.save(new PostImage("file3", post3.getPostId()));
        postContentRepository.save(new PostContent("content1", post1.getPostId()));
        postContentRepository.save(new PostContent("content2", post2.getPostId()));
        postContentRepository.save(new PostContent("content3", post3.getPostId()));
        postRepository.findAll();
    }

    @Test
    void testFindByUrl() {
        // Given
        String urlRoute = "url1";
        // When
        Optional<PostView> postView = postViewRepository.findByUrlRoute(urlRoute);

        // Then
        assertTrue(postView.isPresent());
        assertEquals("url1", postView.get().getUrlRoute());
    }

    @Test
    void testFindAllByStatus() {
        // Given
        String status = "online";
        // When
        List<PostView> postViews = postViewRepository.findAllByStatus(status);
        System.out.println(postViews);
        // Then
        assertEquals(2, postViews.size());
        assertTrue(postViews.stream().allMatch(p -> p.getStatus().equals(status)));
    }


    @Test
    void testFindByFavoriteAndStatusOrderByPublishedAtDesc() {
        // Given
        Boolean favorite = true;
        String status = "online";
        // When
        List<PostView> postViews = postViewRepository.findByFavoriteAndStatusOrderByPublishedAtDesc(favorite, status);
        // Then
        assertEquals(1, postViews.size());
        assertTrue(postViews.stream().allMatch(p -> p.getStatus().equals(status) && p.getFavorite()));
        assertEquals(postViews.stream().sorted(Comparator.comparing(PostView::getPublishedAt).reversed())
                .collect(Collectors.toList()), postViews);
    }

    @Test
    void testFindByUrlAndStatus() {
        // Given
        String urlRoute = "url1";
        String status = "online";
        // When
        Optional<PostView> postView = postViewRepository.findByUrlRouteAndStatus(urlRoute, ONLINE);

        // Then
        assertTrue(postView.isPresent());
        assertEquals("url1", postView.get().getUrlRoute());
        assertEquals("online", postView.get().getStatus());
    }


    @Test
    void testFindSimilarPosts() {
        // When
        List<PostView> postViews = postViewRepository.findSimilarPosts("category2", "online", "url2");

        // Then
        assertEquals(0, postViews.size());
    }

}

