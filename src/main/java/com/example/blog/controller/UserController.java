package com.example.blog.controller;

import com.example.blog.dto.*;
import com.example.blog.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users/posts/")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("likes")
    @ResponseStatus(HttpStatus.CREATED)
    public void addLike(@RequestBody LikeRequest likeRequest) {
        userService.addLike(likeRequest);
    }

    @DeleteMapping("likes")
    public void removeLike(LikeRequest likeRequest) {
        userService.removeLike(likeRequest);
    }

    @PostMapping("comments")
    @ResponseStatus(HttpStatus.CREATED)
    public CommentDTO addComment(@RequestBody CommentToCreate commentToCreate) {
        return userService.addComment(commentToCreate);
    }

    @PutMapping("comments")
    public CommentDTO updateComment(CommentToUpdate commentToUpdate) {
        return userService.updateComment(commentToUpdate);
    }

    @DeleteMapping("comments/{commentId}")
    public void deleteComment(@PathVariable int commentId) {
        userService.deleteComment(commentId);
    }

    @PostMapping("comments/replies")
    @ResponseStatus(HttpStatus.CREATED)
    public CommentReplyDTO addCommentReply(@RequestBody ReplyToCreate replyToCreate) {
        return userService.addCommentReply(replyToCreate);
    }

    @PutMapping("comments/replies")
    public CommentReplyDTO updateCommentReply(@RequestBody ReplyToUpdate replyToUpdate) {
        return userService.updateCommentReply(replyToUpdate);
    }

    @DeleteMapping("comments/replies/{replyId}")
    public void deleteCommentReply(@PathVariable int replyId) {
        userService.deleteCommentReply(replyId);
    }

}
