package com.example.blog.controller;

import com.example.blog.dto.PostRequest;
import com.example.blog.dto.PostDTO;
import com.example.blog.service.AdminService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Log4j2
@RestController
@RequestMapping("/api/admin/posts")
@RequiredArgsConstructor
public class AdminController {

    private final AdminService adminService;

    @GetMapping
    public List<PostDTO> getAllPosts(){
        log.info("------------INTERNAL GET ALL POSTS------------");
        return adminService.getAllPosts();
    }

    @GetMapping(value = "/{postId}")
    public PostDTO getPostByUrlRoute(@PathVariable Integer postId){
        log.info("------------INTERNAL GET POST BY POST_ID------------");
        return adminService.getPostByPostId(postId);
    }

    @PostMapping(consumes = "multipart/form-data")
    @ResponseStatus(HttpStatus.CREATED)
    public PostDTO createBlogPost(@RequestPart MultipartFile image,
                                  @Valid @RequestPart PostRequest postRequest) throws IOException {
        log.info("------------INTERNAL CREATE POST------------");
        return adminService.createBlogPost(postRequest, image);
    }

    @PutMapping(consumes = "multipart/form-data")
    public String updateBlogPost(@RequestPart MultipartFile image,
                                 @Valid @RequestPart PostRequest postRequest) throws IOException {
        log.info("------------INTERNAL UPDATE POST------------");
        return adminService.updateBlogPost(postRequest, image);
    }

    @PutMapping(value = "/{postId}")
    public void setOrRevokeFavorite(
            @PathVariable Integer postId,
            @RequestBody Boolean favorite  ) {
        log.info("Yes");
        adminService.setOrRevokeFavoriteValue(postId, favorite);
    }

    @DeleteMapping(value = "/{urlRoute}", produces = "application/json")
    public String deletePost(@PathVariable String urlRoute){
        log.info("------------DELETE POST------------");
        return adminService.deletePost(urlRoute);
    }
}
