package com.example.blog.controller;

import com.example.blog.dto.PostDTO;
import com.example.blog.service.PostService;
import com.example.blog.service.FileStorageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api/public/posts")
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;
    private final FileStorageService fileStorageService;

    @GetMapping
    public List<PostDTO> getAllOnlinePosts(){
        log.info("------------EXTERNAL GET ALL EXTERNAL POSTS------------");
        return postService.getAllPosts();
    }

    @GetMapping(value="/favorite", produces = "application/json")
    public List<PostDTO> getFavoritePosts(){
        log.info("------------EXTERNAL GET FAVORITE POSTS------------");
        return postService.getFavoritePosts();
    }

    @GetMapping(value="/similar", produces = "application/json")
    public List<PostDTO> getSimilarPosts(@RequestParam("url_route") String urlRoute){
        log.info("------------EXTERNAL GET SIMILAR POSTS------------");
        return null; //externalPostService.getSimilarPosts(urlRoute);
    }
    @GetMapping(value= "/{urlRoute}", produces = "application/json")
    public PostDTO getBlogPostByUrlRoute(@PathVariable String urlRoute){
        log.info("------------EXTERNAL GET POST BY URL------------");
        return postService.getPostByUrlRoute(urlRoute);
    }

    @GetMapping(value="/image/{imageFileName}", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getBlogImagesByImageFileName(@PathVariable String imageFileName) throws IOException {
        return fileStorageService.getImageByFileName(imageFileName);
    }

}
