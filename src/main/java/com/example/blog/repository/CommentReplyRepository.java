package com.example.blog.repository;

import com.example.blog.model.CommentReply;
import com.example.blog.model.PostComment;
import com.example.blog.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CommentReplyRepository extends JpaRepository<CommentReply, Integer> {
    Optional<CommentReply> findByPostCommentAndUserAndReplyText(PostComment postComment, User user, String replyText);

    Optional<CommentReply> findByReplyId(Integer replyId);
}
