package com.example.blog.repository;

import com.example.blog.model.Post;
import com.example.blog.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {
    Optional<Post> findByUrlRoute(String url);

    List<Post> findAllByStatus(Status status);

    List<Post> findByFavoriteAndStatusOrderByPublishedAtDesc(Boolean favorite, Status status);

    Optional<Post> findByUrlRouteAndStatus(String urlRoute, Status status);

    Optional<Post> findByPostId(Integer postId);
    // @Query(nativeQuery = true, value = "SELECT * FROM blog.post WHERE " +
    //        "category_name=?1 AND status=?2 AND url_route!=?3 ORDER BY created_at DESC LIMIT 2" )
    //List<Post> findSimilarPosts(String category, String status, String urlRoute);
}
