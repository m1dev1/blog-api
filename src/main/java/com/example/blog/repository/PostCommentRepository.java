package com.example.blog.repository;

import com.example.blog.model.Post;
import com.example.blog.model.PostComment;
import com.example.blog.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostCommentRepository extends JpaRepository<PostComment, Integer> {
    Optional<PostComment> findByPostAndUserAndComment(Post post, User user, String comment);

    Optional<PostComment> findByCommentId(Integer commentId);
}
