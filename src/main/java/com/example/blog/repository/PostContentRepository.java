package com.example.blog.repository;

import com.example.blog.model.Post;
import com.example.blog.model.PostContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostContentRepository extends JpaRepository<PostContent, Integer> {

    Optional<PostContent> findByPost(Post post);
}
