package com.example.blog.repository;

import com.example.blog.model.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM blog.token t INNER JOIN blog.users u" +
            " ON t.user_id = u.user_id WHERE u.user_id = ?1 " +
            "AND (t.expired = false OR t.revoked = false)")
    List<Token> findAllValidTokenByUserId(Integer userId);
}
