package com.example.blog.repository;

import com.example.blog.model.Post;
import com.example.blog.model.PostImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostImageRepository extends JpaRepository<PostImage, Integer> {
    Optional<PostImage> findByPost(Post post);
}
