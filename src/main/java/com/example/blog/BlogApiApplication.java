package com.example.blog;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
@Slf4j
@SpringBootApplication
public class BlogApiApplication {

    public static void main(String[] args) throws IOException, InterruptedException {
        SpringApplication.run(BlogApiApplication.class, args);
        log.info("BlogApi is up and running");
    }

}
