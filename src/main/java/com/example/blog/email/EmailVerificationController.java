package com.example.blog.email;

import com.example.blog.auth.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api/email")
@RequiredArgsConstructor
public class EmailVerificationController {

    private final AuthenticationService authenticationService;

    @GetMapping(value = "/verify")
    public String verify(@RequestParam String token) {
        return authenticationService.verifyUser(token);
    }
}
