package com.example.blog.email;

import lombok.Getter;

@Getter
public class EmailRequest {

    private String to;
    private String subject;
}
