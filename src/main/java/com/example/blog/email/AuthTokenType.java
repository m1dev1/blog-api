package com.example.blog.email;

public enum AuthTokenType {
    EMAIL_VERIFICATION, RESET_PASSWORD
}
