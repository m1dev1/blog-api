package com.example.blog.email;

import com.example.blog.model.User;
import com.example.blog.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.time.LocalDateTime;
import java.util.UUID;

import static com.example.blog.email.AuthTokenType.EMAIL_VERIFICATION;
import static com.example.blog.email.AuthTokenType.RESET_PASSWORD;

@Log4j2
@Service
@RequiredArgsConstructor
public class EmailService {


    private final TemplateEngine templateEngine;
    private final EmailVerificationTokenRepository verificationTokenRepository;
    private final UserRepository userRepository;

    @Autowired
    private  JavaMailSender javaMailSender;

    public void sendVerificationEmail(User user) {

        String token = createToken(user, EMAIL_VERIFICATION);

        String verificationLink = "http://localhost:8500/api/email/verify?token=" + token;

        // Create Thymeleaf context
        Context context = new Context();
        context.setVariable("verificationLink", verificationLink);

        // Process the Thymeleaf template
        String emailContent = templateEngine.process("email-verification", context);

        sendHtmlEmail(user.getEmail(), "Email verification", emailContent);
    }

    public void sendEmailForNewPassword(User user) {

        String token = createToken(user, RESET_PASSWORD);

        String resetPasswordLink = "http://localhost:8500/api/reset/password?token=" + token;

        // Create Thymeleaf context
        Context context = new Context();
        context.setVariable("resetPasswordLink", resetPasswordLink);

        // Process the Thymeleaf template
        String emailContent = templateEngine.process("reset-password", context);

        sendHtmlEmail(user.getEmail(), "Reset your password", emailContent);
    }

    private String createToken(User user, AuthTokenType resetPassword) {
        String token = UUID.randomUUID().toString();
        AuthToken authToken = new AuthToken();
        authToken.setToken(token);
        authToken.setUser(user);
        authToken.setExpiryDate(LocalDateTime.now().plusMinutes(1));
        authToken.setAuthTokenType(resetPassword);
        verificationTokenRepository.save(authToken);
        return token;
    }


    public void sendEmail(String to, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("loicndankou@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(content);
        javaMailSender.send(message);
    }

    public void sendHtmlEmail(String to, String subject, String htmlContent) {
        MimeMessage message = javaMailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(htmlContent, true);
            javaMailSender.send(message);
        } catch (MessagingException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

}
