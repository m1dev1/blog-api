package com.example.blog.mapper;

import com.example.blog.dto.PostDTO;
import com.example.blog.model.Post;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class PostMapper {
    private static String PATH_URL;

    @Value("${path.external.url}")
    public void setPathUrl(String pathUrl) {
        PATH_URL = pathUrl;
    }

    public static PostDTO map(Post post) {
        return PostDTO.builder()
                .title(post.getTitle())
                .subtitle(post.getSubtitle())
                .createdAt(post.getCreatedAt())
                .publishedAt(post.getPublishedAt())
                .urlRoute(post.getUrlRoute())
                .favorite(post.getFavorite())
                .status(post.getStatus().name()).build();
    }
}
