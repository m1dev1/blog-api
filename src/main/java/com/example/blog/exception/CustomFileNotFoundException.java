package com.example.blog.exception;

public class CustomFileNotFoundException extends RuntimeException{
    public CustomFileNotFoundException(String message){
        super(message);
    }
}
