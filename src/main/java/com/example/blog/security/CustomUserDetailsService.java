package com.example.blog.security;

import com.example.blog.exception.NotFoundException;
import com.example.blog.exception.UserNotVerifiedException;
import com.example.blog.model.User;
import com.example.blog.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Dieser EMail wurde nicht gefunden"));
    }

    public User loadUserByEmail(String email) {
        return userRepository.findByEmail(email)
                .map(user -> {
                    if(user.isVerified()) {
                        return user;
                    } else {
                        String message = String.format(
                                "Email %s is not verified. Please check your email and complete the verification process.",
                                user.getEmail()
                        );
                        log.error(message);
                        throw  new UserNotVerifiedException(message);
                    }
                })
                .orElseThrow(() ->
                        new NotFoundException("No user with email:"+email));
    }

}
