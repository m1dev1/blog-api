package com.example.blog.auth;

import com.example.blog.email.EmailService;
import com.example.blog.email.AuthToken;
import com.example.blog.email.EmailVerificationTokenRepository;
import com.example.blog.exception.AlreadyExistsException;
import com.example.blog.exception.NotFoundException;
import com.example.blog.exception.UserNotVerifiedException;
import com.example.blog.model.TokenType;
import com.example.blog.model.User;
import com.example.blog.repository.TokenRepository;
import com.example.blog.repository.UserRepository;
import com.example.blog.security.CustomUserDetailsService;
import com.example.blog.security.JwtService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.example.blog.email.AuthTokenType.EMAIL_VERIFICATION;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final JwtService jwtService;
    private final CustomUserDetailsService userDetailsService;
    private final EmailService emailService;
    private final AuthenticationManager authenticationManager;
    private final TokenRepository tokenRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final EmailVerificationTokenRepository verificationTokenRepository;


    public void register(Registration registration) {

        String email = registration.getEmail();
        Optional<User> optionalUser = userRepository.findByEmail(email);

        if (optionalUser.isEmpty()) {
            User user = buildUser(registration);
            userRepository.save(user);
            emailService.sendVerificationEmail(user);
        } else {

            User user = optionalUser.get();
            if (user.isVerified()) {
                throw new AlreadyExistsException("User with email:" + email + " already exists");
            } else {

                AuthToken emailToken = user.getAuthTokenList().stream()
                        .filter(authToken -> authToken.getAuthTokenType().equals(EMAIL_VERIFICATION))
                        .findFirst()
                        .orElse(null);

                if (emailToken !=null && isEmailTokenValid(emailToken)) {
                    String message = String.format(
                            "Email %s is not verified. Please check your email and complete the verification process.",
                            user.getEmail()
                    );
                    throw new UserNotVerifiedException(message);
                } else {

                    userRepository.delete(user);
                    User newUser = buildUser(registration);
                    userRepository.save(newUser);
                    emailService.sendVerificationEmail(newUser);
                }
            }
        }
    }

    private User buildUser(Registration registration) {
        return User.builder()
                .username(registration.getUsername())
                .email(registration.getEmail())
                .password(passwordEncoder.encode(registration.getPassword()))
                .userRole(registration.getUserRole())
                .firstname(registration.getFirstname())
                .lastname(registration.getLastname())
                .verified(false)
                .build();
    }

    public String verifyUser(String token) {
        Optional<AuthToken> optionalToken = verificationTokenRepository.findByToken(token);

        if (optionalToken.isPresent()) {
            AuthToken verificationAuthToken = optionalToken.get();

            if (isEmailTokenValid(verificationAuthToken)) {
                User user = verificationAuthToken.getUser();
                user.setVerified(true);
                userRepository.save(user);
                verificationTokenRepository.delete(verificationAuthToken);
                return "verification-success";
            }
        }

        return "verification-error";
    }

    private static boolean isEmailTokenValid(AuthToken verificationAuthToken) {
        return verificationAuthToken.getExpiryDate().isAfter(LocalDateTime.now());
    }


    public String authenticate(AuthenticationRequest request, HttpServletResponse response) {

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword()
                )
        );

        User user = userDetailsService.loadUserByEmail(request.getEmail());
        var jwtToken = jwtService.generateToken(user);
        var refreshToken = jwtService.generateRefreshToken(user);
        revokeAllUserTokens(user);
        saveUserToken(user, jwtToken);

        setRefreshTokenIntoCookie(response, refreshToken);
        return jwtToken;
    }

    private static void setRefreshTokenIntoCookie(HttpServletResponse response, String refreshToken) {
        // Create a cookie for the refresh token
        Cookie cookie = new Cookie("refresh_token", refreshToken);
        cookie.setHttpOnly(true);
        cookie.setPath("/"); // Set the path where the cookie is accessible
        cookie.setSecure(true); // Ensure the cookie is transmitted over HTTPS

        // response.addCookie(cookie);
        response.setHeader("Set-Cookie", "refresh_token=" + refreshToken + "; HttpOnly; Secure; SameSite=Strict; Path=/");
    }

    public String refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String refreshToken = getRefreshTokenFromCookie(request);
        String email = jwtService.extractEmail(refreshToken);
        if (email != null) {
            User user = userDetailsService.loadUserByEmail(email);
            if (user != null && jwtService.isTokenValid(refreshToken, user)) {
                var accessToken = jwtService.generateToken(user);
                revokeAllUserTokens(user);
                saveUserToken(user, accessToken);
                setRefreshTokenIntoCookie(response, refreshToken);
                return accessToken;
            } else {
                throw new NotFoundException("Invalid token");
            }
        } else {
            throw new NotFoundException("No User found");
        }
    }

    public String getRefreshTokenFromCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("refresh_token".equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return "";
    }

    public void sendEmailForNewPassword(String email) {

        User user = userDetailsService.loadUserByEmail(email);
        emailService.sendEmailForNewPassword(user);
    }

    private void saveUserToken(User user, String jwtToken) {
        com.example.blog.model.Token token = new com.example.blog.model.Token();
        token.setToken(jwtToken);
        token.setTokenType(TokenType.BEARER);
        token.setExpired(false);
        token.setRevoked(false);
        token.setUser(user);

        tokenRepository.save(token);
    }

    private void revokeAllUserTokens(User user) {

        List<com.example.blog.model.Token> validUserTokens = tokenRepository.findAllValidTokenByUserId(user.getUserId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }
}
