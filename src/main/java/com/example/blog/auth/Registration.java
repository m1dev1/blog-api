package com.example.blog.auth;

import com.example.blog.model.UserRole;
import lombok.Getter;

@Getter
public class Registration {

    private String username;
    private String email;
    private String password;
    private String firstname;
    private String lastname;
    private UserRole userRole;
}
