package com.example.blog.auth;

import lombok.Getter;

@Getter
public class AuthenticationRequest {

    private String username;
    private String email;
    private String password;
}
