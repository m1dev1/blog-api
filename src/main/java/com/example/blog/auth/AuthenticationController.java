package com.example.blog.auth;

import com.example.blog.email.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthenticationController {

    private final AuthenticationService authenticationService;
    private final EmailService emailService;

    @PostMapping(value = "/login")
    public String authenticate(
            @RequestBody AuthenticationRequest request,
            HttpServletResponse response) {
        return authenticationService.authenticate(request, response);
    }

    @PostMapping(value = "/register")
    public void register(
            @RequestBody Registration registration) {
        authenticationService.register(registration);
    }

    @PostMapping(value = "/refresh-token")
    public String refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        return authenticationService.refreshToken(request, response);
    }

    @PostMapping(value = "/email/password")
    public void sendEmailForNewPassword(@RequestBody String email) {
        authenticationService.sendEmailForNewPassword(email);
    }
}
