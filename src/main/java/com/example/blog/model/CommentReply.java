package com.example.blog.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Getter
@Setter
@Table(name = "comment_reply", schema = "blog")
public class CommentReply {

    @Id
    @Column(name = "reply_id")
    private Integer replyId;

    @ManyToOne
    @JoinColumn(name = "comment_id")
    private PostComment postComment;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "reply")
    private String replyText;

    @Column(name = "replied_at")
    private Date repliedAt;

    @Column(name = "updated_at")
    private Date updatedAt;

    @Column(name = "deleted_at")
    private Date deletedAt;


    @Override
    public String toString() {
        return "CommentReply{" +
                "replyId=" + replyId +
                ", postComment=" + (postComment != null ? postComment.getCommentId() : null) +
                ", user=" + (user != null ? user.getUserId() : null) +
                ", replyText='" + replyText + '\'' +
                ", repliedAt=" + repliedAt +
                ", updatedAt=" + updatedAt +
                ", deletedAt=" + deletedAt +
                '}';
    }
}
