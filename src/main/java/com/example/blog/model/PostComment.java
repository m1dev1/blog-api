package com.example.blog.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(schema="blog", name="post_comment")
public class PostComment {

    @Id
    @Column(name = "comment_id")
    private Integer commentId;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "comment", nullable = false)
    private String comment;

    @Column(name = "added_at", nullable = false)
    private Date addedAt;

    @Column(name = "updated_at")
    private Date updatedAt;

    @Column(name = "deleted_at")
    private Date deletedAt;

    @OneToMany(mappedBy = "postComment")
    private List<CommentReply> replies;

    @Override
    public String toString() {
        return "PostComment{" +
                "commentId=" + commentId +
                ", post=" + (post != null ? post.getPostId() : null) +
                ", user=" + (user != null ? user.getUserId() : null) +
                ", comment='" + comment + '\'' +
                ", addedAt=" + addedAt +
                ", updatedAt=" + updatedAt +
                ", deletedAt=" + deletedAt +
                ", replies=" + (replies != null ? replies.stream().map(CommentReply::getReplyId).toList() : null) +
                '}';
    }
}
