package com.example.blog.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(schema = "blog", name = "image")
@NoArgsConstructor
@AllArgsConstructor
public class PostImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "image_id")
    private Integer imageId;
    @Column(name = "file_name", unique = true)
    private String fileName;
    @OneToOne
    @JoinColumn(name = "post_id")
    private Post post;

    @Override
    public String toString() {
        return "PostImage{" +
                "imageId=" + imageId +
                ", fileName='" + fileName + '\'' +
                ", post=" + (post != null ? post.getPostId() : null) +
                '}';
    }
}
