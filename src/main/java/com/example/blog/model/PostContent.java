package com.example.blog.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(schema="blog", name = "content")
@NoArgsConstructor
public class PostContent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "content_id")
    private Integer contentId;
    @Column(name = "file_name", unique = true)
    private String fileName;
    @OneToOne
    @JoinColumn(name = "post_id")
    private Post post;

    @Override
    public String toString() {
        return "PostContent{" +
                "contentId=" + contentId +
                ", fileName='" + fileName + '\'' +
                ", post=" + (post != null ? post.getPostId() : null) +
                '}';
    }
}
