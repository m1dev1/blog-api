package com.example.blog.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(schema="blog", name="post")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private Integer postId;

    @Column(name = "title")
    private String title;

    @Column(name = "subtitle")
    private String subtitle;

    @Column(name = "created_at")
    private Timestamp createdAt;

    @Column(name = "published_at")
    private Timestamp publishedAt;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name="url_route", unique = true)
    private String urlRoute;

    @Column(name = "favorite")
    private Boolean favorite;

    @ManyToMany
    @JoinTable(
            name = "post_category", schema = "blog",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    private List<Category> categories;

    @OneToOne(mappedBy = "post", cascade = CascadeType.ALL)
    private PostImage postImage;

    @OneToOne(mappedBy = "post", cascade = CascadeType.ALL)
    private PostContent postContent;

    @OneToMany(mappedBy = "post", fetch = FetchType.LAZY)
    private List<PostComment> comments = new ArrayList<>();

    @OneToMany(mappedBy = "post", fetch = FetchType.LAZY)
    private List<PostLike> postLikes = new ArrayList<>();

    @Override
    public String toString() {
        return "Post{" +
                "postId=" + postId +
                ", title='" + title + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", createdAt=" + createdAt +
                ", publishedAt=" + publishedAt +
                ", status='" + status + '\'' +
                ", urlRoute='" + urlRoute + '\'' +
                ", favorite=" + favorite +
                ", categories=" + categories +
                ", postImage=" + (postImage != null ? postImage.getImageId() : null) +
                ", postContent=" + (postContent != null ? postContent.getContentId() : null) +
                ", comments=" + comments  +
                ", postLikes=" + postLikes +
                '}';
    }
}
