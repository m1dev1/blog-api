package com.example.blog.dto;

import lombok.Getter;

@Getter
public class ReplyToUpdate {

    private Integer commentId;
    private String username;
    private String currentReplyText;
    private String newReplyText;
}
