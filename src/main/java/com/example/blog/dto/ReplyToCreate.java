package com.example.blog.dto;

import lombok.Getter;

@Getter
public class ReplyToCreate {

    private Integer commentId;
    private String username;
    private String replyText;
}
