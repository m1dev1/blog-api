package com.example.blog.dto;

import com.example.blog.model.PostComment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CommentDTO {

    private Integer commentId;
    private String username;
    private String urlRoute;
    private String comment;
    private Date addedAt;
    private Date updatedAt;
    private List<CommentReplyDTO> replies;

    public static CommentDTO from(PostComment postComment) {
        List<CommentReplyDTO> commentReplyDTOList = postComment.getReplies().stream()
                .map(CommentReplyDTO::from)
                .toList();
        return new CommentDTO(
                postComment.getCommentId(),
                postComment.getUser().getUsername(),
                postComment.getPost().getUrlRoute(),
                postComment.getComment(),
                postComment.getAddedAt(),
                postComment.getUpdatedAt(),
                commentReplyDTOList);
    }
}
