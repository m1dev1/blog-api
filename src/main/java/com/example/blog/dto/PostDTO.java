package com.example.blog.dto;

import com.example.blog.model.Category;
import com.example.blog.model.Post;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;

import java.sql.Timestamp;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PostDTO {
    private Integer postId;
    private String urlRoute;
    private String title;
    private String subtitle;
    private Timestamp createdAt;
    private Timestamp publishedAt;
    private List<String> categories;
    private String status;
    private Boolean favorite;
    private String imageUrl;
    private String content;
    private int likes;
    private List<CommentDTO> comments;

    public static PostDTO fromPostWithDetails(Post post) {

        List<String> categories = post.getCategories().stream()
                .map(Category::getName)
                .toList();
        List<CommentDTO> commentDTOList = post.getComments().stream()
                .map(CommentDTO::from)
                .toList();
        return PostDTO.builder()
                .postId(post.getPostId())
                .urlRoute(post.getUrlRoute())
                .title(post.getTitle())
                .subtitle(post.getSubtitle())
                .createdAt(post.getCreatedAt())
                .publishedAt(post.getPublishedAt())
                .categories(categories)
                .status(post.getStatus().name())
                .favorite(post.getFavorite())
                .imageUrl("http://localhost:8500/api/public/posts/image/"+ post.getPostImage().getFileName())
                .likes(post.getPostLikes().size())
                .comments(commentDTOList)
                .build();
    }

    public static PostDTO fromPost(Post post) {
        List<String> categories = post.getCategories().stream()
                .map(Category::getName)
                .toList();

        return PostDTO.builder()
                .urlRoute(post.getUrlRoute())
                .title(post.getTitle())
                .subtitle(post.getSubtitle())
                .createdAt(post.getCreatedAt())
                .publishedAt(post.getPublishedAt())
                .categories(categories)
                .status(post.getStatus().name())
                .favorite(post.getFavorite())
                .imageUrl("http://localhost:8500/api/public/posts/image/"+ post.getPostImage().getFileName())
                .likes(post.getPostLikes().size())
                .build();
    }
}


