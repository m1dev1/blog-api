package com.example.blog.dto;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public record PostRequest(
        @NotBlank(message = "urlRoute shouldn't be null or empty")
        @Pattern(regexp = "^([a-z]+-)*[a-z]+$", message = "urlRoute should be in kebab-case format")
        String urlRoute,
        @NotBlank(message = "title shouldn't be null or empty")
        String title,
        @NotNull(message = "subtitle shouldn't be null or empty")
        String subtitle,
        @NotBlank(message = "category shouldn't be null or empty")
        String category,
        @NotBlank(message = "status shouldn't be null or empty")
        String status,
        @NotNull(message = "favorite shouldn't be a boolean")
        Boolean favorite,
        @NotNull(message = "content shouldn't be null or empty")
        String content
        ) {
}
