package com.example.blog.dto;

import lombok.Getter;

@Getter
public class CommentToCreate {

    private String username;
    private String postUrlRoute;
    private String comment;
}
