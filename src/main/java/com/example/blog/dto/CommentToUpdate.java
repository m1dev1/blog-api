package com.example.blog.dto;

import lombok.Getter;

@Getter
public class CommentToUpdate {

    private String username;
    private String postUrlRoute;
    private String currentComment;
    private String newComment;
}
