package com.example.blog.dto;

import lombok.Getter;

@Getter
public class LikeRequest {

    private String username;
    private String postUrlRoute;
}
