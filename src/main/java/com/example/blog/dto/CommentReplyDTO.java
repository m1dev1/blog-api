package com.example.blog.dto;

import com.example.blog.model.CommentReply;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.sql.Date;

@Getter
@AllArgsConstructor
public class CommentReplyDTO {

    private Integer replyId;
    private String username;
    private String reply;
    private Date repliedAt;

    public static CommentReplyDTO from(CommentReply commentReply) {
        return new CommentReplyDTO(
                commentReply.getReplyId(),
                commentReply.getUser().getUsername(),
                commentReply.getReplyText(),
                commentReply.getRepliedAt());
    }
}
