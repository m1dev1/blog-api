package com.example.blog.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Service
@RequiredArgsConstructor
public class FileStorageService {
    private final AmazonS3 amazonS3;

    @Value("${application.bucket.name}")
    public String BUCKET_NAME;
    @Value("${application.production}")
    @Setter
    private Boolean IS_PRODUCTION_ENVIRONMENT;

    private static final String IMAGES_FOLDER = "blog-images/";
    private static final String CONTENT_FOLDER = "blog-content/";

    public void uploadImage(MultipartFile image, String fileName) throws IOException {
        // Create and save Image file locally
        String imagePath = IMAGES_FOLDER + fileName + ".jpg";
        File convertedFile = convertMultiPartFileToFile(image, imagePath);
        // File will be uploaded to s3 bucket only in production
        if(IS_PRODUCTION_ENVIRONMENT){
            uploadFileToS3Bucket(convertedFile, imagePath);
        }
        log.info("Image uploaded to : {}" , imagePath);
    }

    public void uploadContent(String text, String fileName) throws IOException {
        // Create and save content file locally
        String contentPath= CONTENT_FOLDER + fileName + ".txt";
        File convertedFile = convertTextToFile(text, contentPath);
        // File will be uploaded to s3 bucket only in production
        if(IS_PRODUCTION_ENVIRONMENT){
            uploadFileToS3Bucket(convertedFile, contentPath);
        }
        log.info("Text uploaded to : {}" , contentPath);
    }

    private void uploadFileToS3Bucket(File file, String filePath){
        amazonS3.putObject(new PutObjectRequest(BUCKET_NAME,filePath, file));
        if(file.delete()){
            log.info("File:{} deleted", filePath);
        }else {
            log.info("Failed to delete :{}", filePath);
        };
    }

    public byte[] getImageByFileName(String fileName) throws IOException {
        String imagePath = IMAGES_FOLDER + fileName + ".jpg";
        if (IS_PRODUCTION_ENVIRONMENT) {
            S3Object s3Object = amazonS3.getObject(BUCKET_NAME, imagePath);
            S3ObjectInputStream inputStream = s3Object.getObjectContent();
            return readBytesFromStream(inputStream);
        } else {
            Path path = Path.of(imagePath);
            if (!Files.exists(path)) {
                throw new FileNotFoundException("File not found: " + imagePath);
            }
            return readBytesFromDisk(path);
        }
    }

    public String getContentByFileName(String fileName) throws IOException {
        String contentPath = CONTENT_FOLDER + fileName + ".txt";
        if (IS_PRODUCTION_ENVIRONMENT) {
            S3Object s3Object = amazonS3.getObject(BUCKET_NAME, contentPath);
            S3ObjectInputStream inputStream = s3Object.getObjectContent();
            return new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
        } else {
            Path path = Path.of(contentPath);
            if (!Files.exists(path)) {
                throw new FileNotFoundException("File not found: " + contentPath);
            }
            return new String(readBytesFromDisk(path), StandardCharsets.UTF_8);
        }
    }

    byte[] readBytesFromStream(InputStream inputStream) throws IOException {
        try (inputStream) {
            return inputStream.readAllBytes();
        } catch (IOException e) {
            throw new IOException("Error reading file from stream", e);
        }
    }

    byte[] readBytesFromDisk(Path path) throws IOException {
        try {
            return Files.readAllBytes(path);
        } catch (IOException e) {
            throw new IOException("Error reading file from disk", e);
        }
    }

    public void deleteImage(String fileName){
        deleteFile(IMAGES_FOLDER + fileName+ ".jpg");
    }

    public void deleteContent(String fileName){
        deleteFile(CONTENT_FOLDER + fileName + ".txt");
    }

    private void deleteFile(String path){
        if(IS_PRODUCTION_ENVIRONMENT){
            amazonS3.deleteObject(BUCKET_NAME, path);
        }else {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
        }
        log.info("File {} deleted", path);
    }

    private File convertMultiPartFileToFile(MultipartFile multipartFile, String filePath ) throws IOException {
        File file = new File(filePath);
        if(!file.getParentFile().exists() && !file.getParentFile().mkdirs()){
            throw new IOException("Failed to create blog-images directory");
        }
        try {
            FileOutputStream fos= new FileOutputStream(file);
            fos.write(multipartFile.getBytes());
        }catch (IOException e){
            log.info("Error converting {} to file", multipartFile.getOriginalFilename());
            throw e;
        }
        return file;
    }

    private File convertTextToFile(String text, String contentPath) throws IOException {
        Path path = Paths.get(contentPath);
        byte[] textToBytes = text.getBytes();
        if(!Files.exists(path.getParent())){
            Files.createDirectories(path.getParent());
        }
        Files.write(path, textToBytes);
        log.info(path.toString());
        return new File(path.toString());
    }

}
