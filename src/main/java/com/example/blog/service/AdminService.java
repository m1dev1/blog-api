package com.example.blog.service;

import com.example.blog.dto.PostRequest;
import com.example.blog.dto.PostDTO;
import com.example.blog.model.*;
import com.example.blog.exception.AlreadyExistsException;
import com.example.blog.exception.CustomFileNotFoundException;
import com.example.blog.exception.NotFoundException;
import com.example.blog.repository.CategoryRepository;
import com.example.blog.repository.PostContentRepository;
import com.example.blog.repository.PostImageRepository;
import com.example.blog.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.blog.model.Status.ONLINE;

@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
@RequiredArgsConstructor
public class AdminService {
    private final PostRepository postRepository;
    private final CategoryRepository categoryRepository;
    private final PostContentRepository postContentRepository;
    private final PostImageRepository postImageRepository;
    private final FileStorageService fileStorageService;


    public List<PostDTO> getAllPosts() {
        List<PostDTO> postDTOList = postRepository.findAll().stream()
                .map(PostDTO::fromPostWithDetails)
                .collect(Collectors.toList());
        log.info("{} posts was found in DB", postDTOList.size());
        return postDTOList;
    }

    public PostDTO getPostByPostId(Integer postId){
        PostDTO postDTO = postRepository.findByPostId(postId)
                .map(post -> {
                    PostDTO response = PostDTO.fromPostWithDetails(post);
                    String content = getPostContent(post.getPostContent().getFileName());
                    response.setContent(content);
                    return response;
                })
                .orElseThrow(() -> {
                    String message = String.format("The post with the URL route '%s' does not exist.", postId);
                    log.error(message);
                    throw new NotFoundException(message);
                });
        log.info("Post was found in DB");
        return postDTO;
    }

    public String deletePost(String urlRoute) {
        Post post = postRepository.findByUrlRoute(urlRoute)
                .orElseThrow(() -> {
                    String message = String.format("The post with the URL route '%s' does not exist.", urlRoute);
                    log.error(message);
                    return new NotFoundException(message);
                });
        PostImage postImage = post.getPostImage();
        PostContent postContent = post.getPostContent();

        postRepository.delete(post);
        fileStorageService.deleteImage(postImage.getFileName());
        fileStorageService.deleteContent(postContent.getFileName());

        return "Post deleted";
    }

    public PostDTO createBlogPost(PostRequest postRequest, MultipartFile image) throws IOException {
        Timestamp createdAt= Timestamp.valueOf(LocalDateTime.now());
        Timestamp publishedAt=postRequest.status().equals(ONLINE.name()) ? Timestamp.valueOf(LocalDateTime.now()): null ;
        String imageFileName = "image_" + createdAt.toInstant().toEpochMilli();
        String contentFileName= "content_" + createdAt.toInstant().toEpochMilli();

        log.info("Creating new blog post with title: {}", postRequest.title());

        // Check if the post Url already exists
        checkIfUrlAlreadyExists(postRequest.urlRoute());

        // Get the category with the given name
        Category category = getCategoryByName(postRequest.category());

        // Create a new post
        Post post = new Post();
        post.setTitle(postRequest.title());
        post.setSubtitle(postRequest.subtitle());
        post.setStatus(Status.valueOf(postRequest.status()));
        post.setCreatedAt(createdAt);
        post.setPublishedAt(publishedAt);
        post.setFavorite(postRequest.favorite());
        post.setUrlRoute(postRequest.urlRoute());
        post.setCategories(List.of(category));

        PostImage postImage = new PostImage();
        postImage.setFileName(imageFileName);
        postImage.setPost(post);
        PostContent postContent = new PostContent();
        postContent.setFileName(contentFileName);
        postContent.setPost(post);

        post.setPostImage(postImage);
        post.setPostContent(postContent);
        log.info("Post"+post);
        // Save the post, image and content
        try{
            post = postRepository.save(post);
            fileStorageService.uploadImage(image,  imageFileName);
            fileStorageService.uploadContent(postRequest.content(), contentFileName);

        }catch (Exception e){
            log.error("Failed to create blog post due to an error: {}", e.getMessage());
            throw e;
        }
        return PostDTO.fromPostWithDetails(post);
    }

    public String updateBlogPost(PostRequest postRequest, MultipartFile image) throws IOException {
        Post existingPost = postRepository.findByUrlRoute(postRequest.urlRoute())
                .orElseThrow(()->{
                    String message = String.format("The post with the URL route '%s' does not exist.", postRequest.urlRoute());
                    log.error(message);
                    return new NotFoundException(message);
                });
        Timestamp publishedAt = postRequest.status().equals(ONLINE.name())
                && existingPost.getPublishedAt() == null ?
                Timestamp.valueOf(LocalDateTime.now()) : existingPost.getPublishedAt();

        log.info("Updating post with ID: {}", existingPost.getPostId());

        // Check if the post Url already exists
        if(!existingPost.getUrlRoute().equals(postRequest.urlRoute())){
            checkIfUrlAlreadyExists(postRequest.urlRoute());
        }

        // Get the category with the given name
        Category category= getCategoryByName(postRequest.category());

        // Update post
        Post updatedPost = new Post();
        updatedPost.setPostId(existingPost.getPostId());
        updatedPost.setTitle(postRequest.title());
        updatedPost.setSubtitle(postRequest.subtitle());
        updatedPost.setUrlRoute(postRequest.urlRoute());
        updatedPost.setStatus(Status.valueOf(postRequest.status()));
        updatedPost.setCreatedAt(existingPost.getCreatedAt());
        updatedPost.setPublishedAt(publishedAt);
        updatedPost.setFavorite(postRequest.favorite());
        updatedPost.setCategories(List.of(category));

        PostImage postImage = postImageRepository.findByPost(existingPost)
                .orElseThrow(()-> {
                    String message = "Failed to update post. Image not found.";
                    log.error(message);
                    return new NotFoundException(message);
                });
        PostContent postContent = postContentRepository.findByPost(existingPost)
                .orElseThrow(()-> {
                    String message = "Failed to update post. Image not found.";
                    log.error(message);
                    return new NotFoundException(message);
                });

        // update image and content
        try {
            postRepository.save(updatedPost);
            fileStorageService.uploadImage(image, postImage.getFileName() );
            fileStorageService.uploadContent(postRequest.content(), postContent.getFileName());
        } catch (IOException e) {
            log.error("Failed to update due an error: {}", e.getMessage());
            throw e;
        }
        return "Post updated";
    }

    public void setOrRevokeFavoriteValue(Integer postId, boolean isFavorite) {
        postRepository.findByPostId(postId)
                .map(post -> {
                    if(!post.getFavorite().equals(isFavorite)) {

                        post.setFavorite(isFavorite);
                        return postRepository.save(post);
                    }
                    return post;
                }).orElseThrow(() ->
                        new NotFoundException("[postId]: "+postId+". Post doesn't exist"));
    }


    public void checkIfUrlAlreadyExists(String url){
        postRepository.findByUrlRoute(url)
                .ifPresent(p -> {
                    String message = String.format("Failed to create a new post with urlRoute '%s'. The urlRoute already exists.", url);
                    log.error(message);
                    throw new AlreadyExistsException(message);
                });
    }

    public Category getCategoryByName(String category){
        return categoryRepository.findByName(category)
                .orElseThrow(() -> {
                    String message = String.format("Failed to create a new post. Category '%s' not found.",category);
                    log.error(message);
                    return new NotFoundException("Category not found");
                });
    }


    public String getPostContent(String fileName)  {
        try {
            return fileStorageService.getContentByFileName(fileName);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new CustomFileNotFoundException("Error while getting post content");
        }
    }
}
