package com.example.blog.service;

import com.example.blog.dto.PostDTO;
import com.example.blog.exception.CustomFileNotFoundException;
import com.example.blog.exception.NotFoundException;
import com.example.blog.repository.CategoryRepository;
import com.example.blog.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.blog.model.Status.ONLINE;


@Service
@Slf4j
@RequiredArgsConstructor
public class PostService {

    private final PostRepository postRepository;
    private final CategoryRepository categoryRepository;
    private final FileStorageService fileStorageService;

    private final static String STATUS_ONLINE = "online";


    public List<PostDTO> getAllPosts() {
        List<PostDTO> postDTOList = postRepository
                .findAllByStatus(ONLINE).stream()
                .map(PostDTO::fromPost)
                .collect(Collectors.toList());
        log.info("{} online posts in DB", postDTOList.size());
        return postDTOList;
    }

    public PostDTO getPostByUrlRoute(String urlRoute) {
        log.info("Get post with urlRoute: {}", urlRoute);
        PostDTO postDTO = postRepository.findByUrlRouteAndStatus(urlRoute, ONLINE)
                .map(post -> {
                    PostDTO response = PostDTO.fromPostWithDetails(post);
                    response.setContent(getContent(post.getPostContent().getFileName()));
                    return response;
                })
                .orElseThrow(()-> {
                    String message = String.format("The post with the URL route '%s' does not exist.", urlRoute);
                    log.error(message);
                    return new NotFoundException(message);
                });
        log.info("Post was found in DB");
        return postDTO;
    }

    public List<PostDTO> getFavoritePosts() {
        List<PostDTO> postDTOList = postRepository
                .findByFavoriteAndStatusOrderByPublishedAtDesc(true, ONLINE).stream()
                .map(PostDTO::fromPost)
                .collect(Collectors.toList());
        log.info("{} favorite posts was found in DB", postDTOList.size());
        return postDTOList;
    }

    /**
     * Searches for 2 posts that are similar to the one with the given URL route.
     * First, it checks if a post exists with the given URL route.
     * Then, it searches for 2 posts that have the same category as the found post. If there are
     * exactly 2 such posts, it returns them.
     * If there is only one post with the same category as the found post, it returns that post and a random post.
     * If there are no posts with the same category as the found post, it returns 2 random posts.
     * @param urlRoute the URL route of the post to search for
     * @return a list of up to 2 posts that are similar to the one with the given URL route
     */
    /*
    public List<PostResponse> getSimilarPosts(String urlRoute) {
        log.info("Searching for similar posts for: {}", urlRoute);
        List<Post> similarPosts = findSimilarPostsInDB(urlRoute);
        similarPosts = new ArrayList<>(similarPosts);

        int size = similarPosts.size();

        if (size == 0) {
            similarPosts = postViewRepository.findAll()
                    .stream()
                    .filter(post -> !post.getUrlRoute().equals(urlRoute))
                    .limit(2)
                    .collect(Collectors.toList());
        } else if (size == 1) {
            PostView firstSimilarPost = similarPosts.get(0);
            similarPosts.addAll(postViewRepository.findAll()
                    .stream()
                    .filter(post -> !post.getUrlRoute().equals(urlRoute)
                            && !post.getUrlRoute().equals(firstSimilarPost.getUrlRoute()))
                    .limit(1).toList());
        }

        List<PostResponse> postResponseList = similarPosts.stream()
                .map(this::mapToPostResponse)
                .collect(Collectors.toList());
        log.info("Similar posts found in the database: {}", postResponseList.size());
        return postResponseList;
    }

    private List<Post> findSimilarPostsInDB(String urlRoute) {
        return postRepository.findByUrlRouteAndStatus(urlRoute,STATUS_ONLINE)
                .map(post ->{
                    return postRepository.findSimilarPosts(
                            post.(), "online", urlRoute);
                })
                .orElseThrow(()-> {
                    String message = String.format("The post with the URL route '%s' does not exist.", urlRoute);
                    log.error(message);
                    throw new NotFoundException(message);
                });
    }*/


    String getContent(String content)  {
        try {
            return fileStorageService.getContentByFileName(content);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new CustomFileNotFoundException("Error while getting post content");
        }
    }

}
