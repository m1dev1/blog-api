package com.example.blog.service;

import com.example.blog.dto.*;
import com.example.blog.model.*;
import com.example.blog.exception.NotFoundException;
import com.example.blog.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;

import static com.example.blog.model.Status.ONLINE;

@Log4j2
@Service
@RequiredArgsConstructor
public class UserService {

    private final PostCommentRepository postCommentRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final CommentReplyRepository commentReplyRepository;
    private final PostLikeRepository postLikeRepository;

    public void addLike(LikeRequest request) {

        User user = findUserByUsername(request.getUsername());
        Post post = findPostByUrlRoute(request.getPostUrlRoute());

        PostLike postLike = new PostLike();
        postLike.setUser(user);
        postLike.setPost(post);

        postLike = postLikeRepository.save(postLike);
    }

    public void removeLike(LikeRequest request) {

        User user = findUserByUsername(request.getUsername());
        Post post = findPostByUrlRoute(request.getPostUrlRoute());

        PostLike postLike = postLikeRepository.findByUserAndPost(user, post)
                .orElseThrow(() -> {
                    String message =
                            String.format("PostLike not found with username: %s and URL route:%s",
                                    request.getUsername(),
                                    request.getPostUrlRoute());
                    log.error(message);
                    return new NotFoundException(message);
                });

        postLikeRepository.delete(postLike);
    }

    public CommentDTO addComment(CommentToCreate commentToCreate) {
        User user = findUserByUsername(commentToCreate.getUsername());
        Post post = findPostByUrlRoute(commentToCreate.getPostUrlRoute());

        PostComment postComment = new PostComment();
        postComment.setPost(post);
        postComment.setUser(user);
        postComment.setComment(commentToCreate.getComment());
        postComment.setAddedAt(Date.valueOf(LocalDate.now()));

        postComment = postCommentRepository.save(postComment);
        return CommentDTO.from(postComment);
    }

    public CommentDTO updateComment(CommentToUpdate commentToUpdate) {
        User user = findUserByUsername(commentToUpdate.getUsername());
        Post post = findPostByUrlRoute(commentToUpdate.getPostUrlRoute());

        String currentComment = commentToUpdate.getCurrentComment();
        PostComment postComment = findPostComment(post, user, currentComment);
        postComment.setComment(commentToUpdate.getNewComment());
        postComment.setUpdatedAt(Date.valueOf(LocalDate.now()));

        postComment = postCommentRepository.save(postComment);
        return CommentDTO.from(postComment);
    }

    public void deleteComment(int commentId) {
        PostComment postComment = findPostComment(commentId);
        postComment.setDeletedAt(Date.valueOf(LocalDate.now()));
        postCommentRepository.save(postComment);
    }

    public CommentReplyDTO addCommentReply(ReplyToCreate replyToCreate) {
        User user = findUserByUsername(replyToCreate.getUsername());
        PostComment postComment = findPostComment(replyToCreate.getCommentId());

        CommentReply commentReply = new CommentReply();
        commentReply.setPostComment(postComment);
        commentReply.setReplyText(replyToCreate.getReplyText());
        commentReply.setRepliedAt(Date.valueOf(LocalDate.now()));

        commentReply = commentReplyRepository.save(commentReply);
        return CommentReplyDTO.from(commentReply);
    }

    public CommentReplyDTO updateCommentReply(ReplyToUpdate replyToUpdate) {
        User user = findUserByUsername(replyToUpdate.getUsername());
        PostComment postComment = findPostComment(replyToUpdate.getCommentId());

        String currentReplyText = replyToUpdate.getCurrentReplyText();
        CommentReply commentReply = commentReplyRepository
                .findByPostCommentAndUserAndReplyText(postComment, user, currentReplyText)
                .orElseThrow(() -> handleEntityNotFound("CommentReply", "reply", currentReplyText));
        commentReply.setReplyText(replyToUpdate.getNewReplyText());
        commentReply.setUpdatedAt(Date.valueOf(LocalDate.now()));

        commentReply = commentReplyRepository.save(commentReply);
        return CommentReplyDTO.from(commentReply);
    }

    public void deleteCommentReply(int replyId) {
        CommentReply commentReply = commentReplyRepository.findByReplyId(replyId)
                .filter(c -> c.getDeletedAt() == null)
                .orElseThrow(()
                        -> handleEntityNotFound("CommentReply", "replyId", String.valueOf(replyId)));
        commentReply.setDeletedAt(Date.valueOf(LocalDate.now()));
        commentReplyRepository.save(commentReply);
    }

    private User findUserByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> handleEntityNotFound("User", "username", username));
    }

    private Post findPostByUrlRoute(String urlRoute) {
        return postRepository.findByUrlRouteAndStatus(urlRoute, ONLINE)
                .orElseThrow(() -> handleEntityNotFound("Post", "URL route", urlRoute));
    }

    private PostComment findPostComment(Post post, User user, String comment) {
        return postCommentRepository.findByPostAndUserAndComment(post, user, comment)
                .orElseThrow(() -> handleEntityNotFound("PostComment", "comment", comment));
    }
    private PostComment findPostComment(int commentId) {
        return postCommentRepository.findByCommentId(commentId)
                .filter(p -> p.getDeletedAt() == null)
                .orElseThrow(()
                        -> handleEntityNotFound("PostComment", "commentId", String.valueOf(commentId)));
    }

    private NotFoundException handleEntityNotFound(String entity, String attribute, String value) {
        String message = String.format("%s not found with %s: %s", entity, attribute, value);
        log.error(message);
        return new NotFoundException(message);
    }

}
