CREATE SCHEMA IF NOT EXISTS blog;

CREATE TABLE  IF NOT EXISTS blog.post (
                      post_id BIGINT AUTO_INCREMENT PRIMARY KEY,
                      title VARCHAR(255) NOT NULL,
                      subtitle VARCHAR(255),
                      created_at TIMESTAMP NOT NULL,
                      published_at TIMESTAMP,
                      status VARCHAR(255) NOT NULL,
                      url_route VARCHAR(255) NOT NULL UNIQUE ,
                      favorite BOOLEAN
);

CREATE TABLE  IF NOT EXISTS blog.category(
      category_id BIGINT AUTO_INCREMENT PRIMARY KEY,
      category_name VARCHAR(255) NOT NULL
);

CREATE TABLE  IF NOT EXISTS blog.image(
    image_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    file_name VARCHAR (255) NOT NULL,
    post_id BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS blog.content(
    content_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    file_name VARCHAR (255) NOT NULL,
    post_id BIGINT NOT NULL
);


CREATE TABLE IF NOT EXISTS blog.users (
    user_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR (255) UNIQUE NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    user_role VARCHAR (255) NOT NULL,
    firstname VARCHAR(225) NOT NULL,
    lastname VARCHAR(225),
    verified boolean NOT NULL
);

CREATE TABLE IF NOT EXISTS blog.post_comment(
    comment_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    post_id BIGINT REFERENCES post(post_id),
    user_id BIGINT REFERENCES users(user_id),
    comment VARCHAR(225) NOT NULL,
    added_at DATE NOT NULL,
    updated_at DATE NOT NULL,
    deleted_at DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS blog.comment_reply(
    reply_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    comment_id BIGINT REFERENCES post_comment(comment_id),
    user_id BIGINT REFERENCES users(user_id),
    reply VARCHAR(225) NOT NULL,
    replied_at DATE NOT NULL,
    updated_at DATE NOT NULL,
    deleted_at DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS blog.token(
    token_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    user_id BIGINT REFERENCES users(user_id),
    token VARCHAR(225) NOT NULL,
    token_type VARCHAR(225) NOT NULL,
    revoked BOOLEAN NOT NULL,
    expired BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS blog.auth_token(

);