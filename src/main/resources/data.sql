CREATE SCHEMA IF NOT EXISTS blog;
--AUTH: username=admin , password=password
INSERT INTO blog.users (username, email, password, user_role, firstname, lastname, verified)
VALUES ('admin', 'admin@mail.com', '$2a$12$rx.KiG6x38pvEiGh4e1Yg.smcDUh1thjY6OGpQLIt/mb9ld1DhKj6', 'ROLE_ADMIN', 'Lea', null, true);
--DEFAULT VALUES
INSERT INTO blog.category (category_name) VALUES ('Technology'), ('Travel'), ('Food');
INSERT INTO blog.post (title, subtitle, created_at, published_at, status, url_route, favorite)
VALUES ('How to code in Java', 'Learn the basics of Java programming language', '2022-10-10 10:45:12', NULL, 'ONLINE', 'how-to-code-in-java', false),
       ('My Trip to Japan', 'A journey to the land of the rising sun', '2022-11-01 15:14:07', '2022-11-05 10:00:00', 'ONLINE', 'my-trip-to-japan', true),
       ('The Best Pizza Places in NYC', NULL, '2022-12-20 12:10:33', NULL, 'OFFLINE', 'best-pizza-places-in-nyc', false);

INSERT INTO blog.image (file_name, post_id)
VALUES ('image_1665391512000', 1),
       ('image_1667312047000', 2),
       ('image_1671534633000', 3);

INSERT INTO blog.content (file_name, post_id)
VALUES ('content_1665391512000', 1),
       ('content_1667312047000', 2),
       ('content_1671534633000', 3);

CREATE TABLE IF NOT EXISTS blog.post_category (
    post_id BIGINT REFERENCES post(post_id),
    category_id BIGINT REFERENCES category(category_id)
);

INSERT INTO blog.post_category (post_id, category_id)
VALUES (1, 1),
        (1, 3),
        (2, 1),
        (2, 3),
        (3, 2);
