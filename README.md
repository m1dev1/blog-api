# Documentation: Software architecture

## **Introduction**

This backend service is built using Java 17, Spring Boot, and Gradle. It provides a REST API for a blog website and uses Spring Security to secure the API endpoints. For local development, H2 database is used and for production deployment, PostgreSQL is used. Images are stored in an S3 bucket. The service is deployed to an EC2 instance and is set up to run in a Docker container.

![Software architecture](software_architecture.jpg)

## **Getting Started**

To get started with this backend service, follow these steps:

1. Clone the project from the Git repository.
2. Install Java 17 and  Gradle on your local machine.
3. Open the project in your IDE of choice.
4.  Run the service locally using the **`./gradlew bootRun`** command. The service should be accessible at **`http://localhost:8085`**.

## **API Endpoints**

This service provides the following REST API endpoints:

### External endpoints

You can use a tool like Postman or cURL to make API requests to the service. By default, you can access the external endpoints without authentication. These endpoints are designed for fetching public data such as blog posts, categories, and publication date.

- **`GET /posts`**: Returns a list of all blog posts.

```json
cURL GET http://localhost:8085/external/blog/posts
```

### Internal endpoints

To access the internal endpoints, you need to authenticate yourself with Basic Authentication. These endpoints are designed for creating, updating, and deleting posts, categories, and authors. Requests that start with the following prefix require Basic Authentication:

- **`http://localhost:8085/internal/**`**

To access the internal endpoints, you need to provide your username and password. The credentials are then verified against the stored user information in the **`users`** table of the database.

- **`POST /posts`**: Creates a new blog post.
- **`PUT /posts/{postUrl}`**: Updates an existing blog post by postUrl.
- **`DELETE /posts/{postUrl}`**: Deletes a blog post by postUrl.

By using different authentication mechanisms for internal and external endpoints, the service provides a secure solution for the blog website.

### Example

To authenticate, use the following credentials:

Username: `admin` Password: `password`

`POST /api/posts`

This endpoint creates a new post

```json
curl --location --request POST 'http://localhost:8500/internal/blog/posts' \
--header 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=' \
--form 'image=@"/path/to/image.jpg"' \
--form 'postRequest={"title":"New Title", "subtitle":"New subtitle", "category":"Travel", "urlRoute":"new-title","status":"online", "content":"<p>New content</p>","favorite":"true"};type=application/json'
```

API documentation can be found at **[http://localhost:8085/v3/api-docs](http://localhost:8085/v3/api-docs)**

You can import it into tools like Swagger or Postman.

## **Security**

This service is secured with Spring Security. The API resources have two different types of endpoints: external and internal. The external endpoints are open to the public, while the internal endpoints are protected and require Basic Authentication.

Spring Security uses role-based authorization to limit access to the internal endpoints. The service has two roles:

- **`ROLE_ADMIN`**: This role can perform any action on the API.
- **`ROLE_USER`**: This role can only create and edit blog posts.

By securing the service with Spring Security and implementing role-based authorization, the service provides a secure and controlled environment for managing the blog.

## **Storage**

### **Database**

The service uses two databases. For local development, the H2 database is used, while for production deployment, PostgreSQL is used. Database migration is done using Flyway. The following tables are used:

- **`posts`**: Stores information about the blog posts, such as title, subtitle, content, publication date, image URL, status, and whether the post is a favorite or not.
- **`category`**: Stores the names of the categories.
- **`image`**: Stores the filename for the image of each post.
- **`content`**: Stores the filename for the content of each post.
- **`users`**: Stores information about the users, such as username, password, and role.

### **Filesystem**

In the non-production environment, the image and content files are stored in the project root under the **`/blog-images`** and **`/blog-content`** directories, respectively. Rather than storing the files directly in the database, the service stores them in the filesystem to improve performance. It is important to choose the right filesystem for the application's needs. In production, one of the most popular and powerful cloud filesystems is Amazon S3. One challenge is to keep the database and the filesystem in sync to avoid inconsistencies.

## **Deployment**

The service is deployed to an EC2 instance and is set up to run in a Docker container. The Docker image is built using a Dockerfile, which installs all the necessary dependencies and sets up the environment. The Docker container is started using the **`docker run`** command with two replicas. The service is started automatically when the Docker container is started.

## **Conclusion**

This backend service offers a secure and controlled environment for managing a blog, with separate internal and external endpoints.By using Docker containers to deploy it to an EC2 instance, the service is highly scalable.